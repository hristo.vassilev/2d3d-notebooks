import os
import numpy as np
from PIL import Image
from bcf.v2 import bcfxml
from bcf.v2.data import (HeaderFile, 
                         Header, 
                         Comment,
                         Viewpoint,
                         Component,
                         Components,
                         ComponentVisibility,
                         ViewSetupHints,
                         OrthogonalCamera,
                         Direction, Point
                        )
import datetime

def create_bcf(element_guids, 
               bcf_filepath, 
               setVisible = False,
               title = '2d3d',
               description = 'empty description',
               status = 'in progress',
               topic_type = 'Issue',
               due_date = datetime.datetime(2022,7,1),
               creation_author = 'Student_X',
               assigned_to = 'Student_Y',
               labels = ['Architecture','Mechanical'],
               comment = 'Insert comment here',
               comment_author = 'Student_X',
              ):

    mybcf = bcfxml.BcfXml()
    mybcf.new_project()
    mybcf.project.name = "bcf_test"


    for element_guid  in element_guids:
    
        # create a new topic (topic = issue)
        my_topic = mybcf.add_topic()
        my_topic.title = title
        my_topic.description = description
        my_topic.topic_status = status
        my_topic.topic_type = topic_type
        my_topic.due_date = due_date
        my_topic.creation_author = creation_author
        my_topic.assigned_to = assigned_to

        my_topic.labels = labels

        my_comment = Comment()
        my_comment.comment = comment
        my_comment.author = comment_author
        my_comment.modified_date = datetime.datetime.now()


        mybcf.add_comment(my_topic,my_comment)


        header_file = HeaderFile()
        header_file.reference = bcf_filepath
        header_file.filename = bcf_filepath

        header = Header()
        header.files.append(header_file)

        my_topic.header = header
        mybcf.edit_topic(my_topic)

        # add viewpoint
        viewpoint = Viewpoint()
        viewpoint.components = Components()

        # select element
        component = Component()
        component.ifc_guid = element_guid
        viewpoint.components.selection.append(component)

        # ensure elements are visible
        viewpoint.components.visibility = ComponentVisibility()
        viewpoint.components.view_setup_hints = ViewSetupHints()
        viewpoint.components.view_setup_hints.spaces_visible = True
        viewpoint.components.view_setup_hints.space_boundaries_visible = True
        viewpoint.components.view_setup_hints.openings_visible = True

        viewpoint.components.visibility.default_visibility = setVisible

        # Camera at origin
        camera = OrthogonalCamera()
        camera.camera_view_point.x = 0.0
        camera.camera_view_point.y = 0.0
        camera.camera_view_point.z = 0.0

        camera.camera_direction.x = 1.0
        camera.camera_direction.y = 0.0
        camera.camera_direction.z = 0.0

        camera.camera_up_vector.x = 0.0
        camera.camera_up_vector.y = 0.0
        camera.camera_up_vector.z = 1.0

        camera.view_to_world_scale = 20.0

        viewpoint.orthogonal_camera = camera

        # create fake snapshot
        snap_fp = os.path.join(mybcf.filepath, my_topic.guid,'snapshot.jpeg')
        snap_im = [[0 for x in range(10)] for y in range(10)]
        snap_im = Image.fromarray(np.array(snap_im).astype(float)).convert('RGB')
        snap_im.save(snap_fp)
        viewpoint.snapshot = 'snapshot.jpeg'


        mybcf.add_viewpoint(my_topic, viewpoint)
        my_comment.viewpoint = viewpoint


    # save bcf file
    bcf_filepath = os.path.abspath(bcf_filepath)
    mybcf.save_project(bcf_filepath)
